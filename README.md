# mapbox-nuxt
using mapbox to put markers on the map and save its locations intolocalStorage.

NOTE:
-------
I should be implementing a basic auth system, but i didn't although I am able to do it using:
Jwt + Joi for validation + csrf protection +...etc.

I have did the challenging work in the front-end, it was more fun. this is the first time I work
with any kind of map.

all the design herein, is made by me. it's not responsive yet but it's easy to be (it's all about 5 min xD).

# Credits
- TailwindCSS
- animate.css
- Nuxt.js
- Express.js
- Sequelize (MySQL ORM)
- Jwt web token (not used yet)
- axios
- mapbox web cdn

> My MapBox + Nuxt.js & Node.js project

## Build Setup

``` bash
# install dependencies
$ npm run install
```

### for developement or playing
you must create a database with these info

``` bash
{
  "jwtPrivateKey": "jwtPrivateKey",
  "mysql_db": "testmapbox",
  "mysql_user": "root",
  "mysql_password": "",
  "host": "127.0.0.1",
  "port": "3000"
}
```

1- setup and create your tables
``` bash
# create MySQL tables
$ npm run setup
```

2- set your access token (nuxt.config.js)
Feel free to skip this step because by default you find my token, and works fine.
``` bash
# nuxt.config.js
env: {
  mapboxgl: {
    accessToken: 'pk.eyJ1IjoiaGFtemFub3VhbGkiLCJhIjoiY2swNGYxdTBoMDBheTNuczcwYndoYmY0biJ9.MgctyzpPNx1RCIi8GdXpog'
  }
},
```

3- build it locally
``` bash
# serve with hot reload at localhost:3000
$ npm run dev
```

### for production
not ready yet..
