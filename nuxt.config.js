
module.exports = {
  mode: 'universal',

  env: {
    mapboxgl: {
      accessToken: 'pk.eyJ1IjoiaGFtemFub3VhbGkiLCJhIjoiY2swNGYxdTBoMDBheTNuczcwYndoYmY0biJ9.MgctyzpPNx1RCIi8GdXpog'
    }
  },

  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/native_design/assets/style.css',
    '@/native_design/assets/tailwindcss/dist/tailwind.min.css',
    '@/native_design/assets/@fortawesome/fontawesome-free/css/all.min.css',
    '@/native_design/assets/animate.css/animate.min.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    //'@nuxtjs/tailwindcss',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
