const express = require('express')
const app = express()

/**
 * by doing this, i don't need to do this
 * require('somthing')(app)
 *                    ^^^^^^
 * or
 * require('somthing')(express)
 *                    ^^^^^^^^^^
 */
module.exports = { app, express }