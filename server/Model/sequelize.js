const Sequelize = require('sequelize');

/*
 * change the config dir
 * because we may encounter some issues when working on different OS (windows, ubuntu..etc)
 */
const path = require('path');
process.env['NODE_CONFIG_DIR'] = path.join(__dirname, '../config');
const config = require('config');

/**
 * Here I should be handling the env variables and checking wether it's defined
 * by the developer or not
 */
 /* 
 if(!config.has('db_name')) {
    throw Error('ENV Variables: db_name is not defined.');
    process.exit(1);
 } else if(... */

/**
 * connecting to database
 */
const sequelize = new Sequelize(config.get('mysql_db'), config.get('mysql_user'), config.get('mysql_password'), {
  dialect: 'mysql'
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    throw err;
    console.error('Unable to connect to the database:', err);
    process.exit(1);
  });

module.exports = {
  Sequelize, sequelize
};