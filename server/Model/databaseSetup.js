const { Sequelize, sequelize } = require('./sequelize');

/**
 * Require all Models here
 **************************************************************/
const User = require('./UserModel');
//const Location = require('./LocationModel');



/***************************************************************
 * Creating tables
 */
sequelize.sync();