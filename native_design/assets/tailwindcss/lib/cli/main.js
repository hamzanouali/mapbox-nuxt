"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = run;

var _commands = _interopRequireDefault(require("./commands"));

var utils = _interopRequireWildcard(require("./utils"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * CLI application entrypoint.
 *
 * @param {string[]} cliArgs
 * @return {Promise}
 */
function run(cliArgs) {
  return new Promise((resolve, reject) => {
    const params = utils.parseCliParams(cliArgs);
    const command = _commands.default[params[0]];
    const options = command ? utils.parseCliOptions(cliArgs, command.optionMap) : {};
    const commandPromise = command ? command.run(params.slice(1), options) : _commands.default.help.run(params);
    commandPromise.then(resolve).catch(reject);
  });
}