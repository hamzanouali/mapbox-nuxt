"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = compile;

var _postcss = _interopRequireDefault(require("postcss"));

var utils = _interopRequireWildcard(require("./utils"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Compiler options
 *
 * @typedef {Object} CompileOptions
 * @property {string} inputFile
 * @property {string} outputFile
 * @property {array} plugins
 */
const defaultOptions = {
  inputFile: null,
  outputFile: null,
  plugins: []
  /**
   * Compiles CSS file.
   *
   * @param {CompileOptions} options
   * @return {Promise}
   */

};

function compile(options = {}) {
  const config = { ...defaultOptions,
    ...options
  };
  const css = utils.readFile(config.inputFile);
  return new Promise((resolve, reject) => {
    (0, _postcss.default)(config.plugins).process(css, {
      from: config.inputFile,
      to: config.outputFile
    }).then(resolve).catch(reject);
  });
}